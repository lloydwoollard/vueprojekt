﻿var date = new Date();

var indexBody = new Vue({
    el: '#indexBody',
    data: {
        nameInput: 'Test',
        descriptionInput: 'Test Description',
        toDoBy: new Date(),
        toDoCreated: new Date(),
        // Makes a default to do in the array with an id of 0
        toDos: [new ToDo("Test", "Test Description", new Date(), 0)]
    },
    components: {
        // The name Felix was from Christoph Denkers and stuck, but it's just a button
        'felix': {
            template: '<button v-on:click="deleteToDo(id)" style="background-color:red;font-size:16px" >Delete</button>',
            prop: {
                id: Number,
            },
            methods: {
                deleteToDo: function (id) {
                    var index = 0;
                    for (var i = 0; i < indexBody.toDos.length; i++) {
                        if (indexBody.toDos[i].id === id) {
                            index = i;
                        }
                    }
                    indexBody.toDos.splice(index, 1);
                    console.log("deleted, index of " + id);
                }
            },
        }
    },
    methods: {
        addToDo: function () {
            // Error if text fields are empty
            if (this.nameInput.trim() === "" || this.descriptionInput.trim() === "") {
                alert("Input is empty");
                return;
            }
            
            // Finds the highest id
            var index = 0;
            for (var i = 0; i < indexBody.toDos.length; i++) {
                if (index <= indexBody.toDos[i].id) {
                    index = indexBody.toDos[i].id + 1;
                }
            }

            indexBody.toDos.push(new ToDo(this.nameInput, this.descriptionInput, this.toDoBy, index));
            console.log("added");
        },
        saveList: function () {
            localStorage.setItem("todo", JSON.stringify(indexBody.toDos));
            console.log("saved, " + indexBody.toDos);
        },
        loadToDOs: function () {
            indexBody.toDos = JSON.parse(localStorage.getItem("todo"));
            // JSON.parse makes the dates into strings, so converts them back to dates
            for (var i = 0; i < indexBody.toDos.length; i++) {
                indexBody.toDos[i].toDoBy = new Date(indexBody.toDos[i].toDoBy);
                indexBody.toDos[i].toDoCreated = new Date(indexBody.toDos[i].toDoCreated);
            }
            console.log(indexBody.toDos);
        },
    }
})

// ToDo Object
function ToDo(name, description, toDoBy, id) {
    this.name = name;
    this.description = description;
    this.toDoBy = new Date(toDoBy);
    this.toDoCreated = new Date(date);
    this.id = id;

    this.log = function () {
        console.log(this.toString());
    };

    this.toString = function () {
        return this.name + " " + this.description + " " + this.toDoBy + " " + this.toDoCreated;
    };
}